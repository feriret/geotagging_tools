#!/bin/bash
input=$1 #text file with ID longitude and latitude
files=$2 #path to imagecd m directory

var=($(ls $2))
bfname=$(echo ${var[0]}|cut -c 1-4) #basename files in directory $2

#READ textfile
while IFS=" " read -r ID LAT LON line
do
  #Stereo
  echo $files"$ID" Latitude = "$LAT" Longitude = "$LON"

  # For images recorded in Paris, Barcelona, Antibes and Genoa
  exiftool $files"$ID".jpg -gpslatitude=$LAT  -gpslongitude=$LON -gpslatituderef=N -gpslongituderef=E -model=OnePlus9 -make=OnePlus9_BX # OnePlus9 images
  #exiftool $files"$ID".jpg -gpslatitude=$LAT  -gpslongitude=$LON -gpslatituderef=N -gpslongituderef=E -model=GoProMAX -make=GoPro     #GoProMax images
  #exiftool $files"$ID".jpg -gpslatitude=$LAT  -gpslongitude=$LON -gpslatituderef=N -gpslongituderef=E -model=Insta -make=Insta360     #Insta360 images


  # For images recorded in Lisbon
  exiftool mapillary_frames/"$ID".jpg -gpslatitude=$LAT  -gpslongitude=$LON -gpslatituderef=N -gpslongituderef=W -model=OnePlus9 -make=OnePlus9_BX   
  rm $files*.jpg_original

done < "$input"


