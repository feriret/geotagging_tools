#! /bin/bash

a=1
echo Working on it...
for i in *.jpg; do
  bfname=$(echo $i|cut -c 1-4)
  echo $(printf "%06d.jpg" "$a") 
  new=$(printf "%06d.jpg" "$a")   
  mv -i -- "$i" "$new"
  let a=a+1
done
echo DONE!
