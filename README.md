# Geotaggin tools
## syncronize videos
WARNING: MAKE A BACKUP OF DATA BEFORE PROCESSING BECAUSE SCRIPT DELETES ORIGINAL DATA

conda activate py38
python /DATA/scripts_python/sync_videos_by_audio.py

## Extract images
//For bagpack V3
python /DATA/scripts_python/Backpack-Utilities/extract_images_from_video_scaled.py -i $1 -s 1

// Previous versions
conda activate python3.9
mapillary_tools video_sample --video_sample_interval 0.03 {for_each_video}/*.mp4

## RUN ORBSLAM or DSO orbslam
Output -> CameraTrajectory.txt

## Aligning to GPS (meshlab)
For Version Bagpack 3
python /DATA/scripts_python/scripts_tum/evaluate_ate_auto_scale.py GPS.txt SLAM.txt 
output-> Transformation matrix [R,t,scale]

Previous methods 
Use alignment tool meshlab

## Geotag
This script provides the tools to geotag the Latitude, Longitude of imagery extracted by mapillary_tools sample_video. The input is a path to the images directory and a CSV file containing the ID of the image, its latitude and longitude as follows:

```
ID latitude longitude
000001 41.37582 2.177602
000002 41.37582 2.177602
000003 41.37582 2.177602
000004 41.37582 2.177602
000005 41.37582 2.177602
...
```

## How to use
Uncomment the line corresponding to the city where the sequence was recorded

bash {PATH}/geotag_images.sh tags_img.csv {PATH_TO_IMAGES}


## Upload to mapillary
conda activate python3.9
mapillary_tools.ext process_and_upload "path/to/images" --user_name "mapillary_user" --organization_key $YourOrganizationKey


## Retrieve 
